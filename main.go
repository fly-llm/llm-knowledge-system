package main

import (
	_ "llm-knowledge-system/internal/packed"

	"github.com/gogf/gf/v2/os/gctx"

	"llm-knowledge-system/internal/cmd"
)

func main() {
	cmd.Main.Run(gctx.GetInitCtx())
}
