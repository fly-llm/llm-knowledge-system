#!/bin/sh

apt update && apt install git-lfs -y

cd docker-compose/models

# 下载 chatlgm3
if [ ! -d "chatglm3-6b" ]; then
    echo "start downloading chatglm3-6b"
    git clone https://www.modelscope.cn/ZhipuAI/chatglm3-6b.git
fi

# 下载 bge-large-zh
if [ ! -d "bge-large-zh" ]; then
    echo "start downloading bge-large-zh"
    git clone https://www.modelscope.cn/AI-ModelScope/bge-large-zh.git
fi


cd ..
echo "docker-compose build and run"

docker-compose up -d
